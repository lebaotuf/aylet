/* aylet 0.5, a .AY music file player.
 * Copyright (C) 2018-2019 Ábel Futó. See main.c for licence.
 *
 * drv-pulse.c - PulseAudio sound code
 */

#define BUFSIZE 1024

#include <pulse/simple.h>
#include <pulse/error.h>
#include <stdio.h>

#include "sound.h"
#include "driver.h"

static pa_simple *s;

int driver_init(unsigned int *freqptr, int *stereoptr)
{
	int error;
	pa_sample_spec ss;

	ss.format = PA_SAMPLE_S16NE;
	ss.channels = (*stereoptr) ? 2 : 1;
	ss.rate = (*freqptr);

	s = pa_simple_new(NULL,
			  "Aylet",		// Our application's name.
			  PA_STREAM_PLAYBACK,
			  NULL,			// Use the default device.
			  "AY chiptune player",	// Description of our stream.
			  &ss,			// Our sample format.
			  NULL,			// Use default channel map
			  NULL,			// Use default buffering attributes.
	                  &error
	                  );

	if (!s) {
		fprintf(stderr, "%s\n", pa_strerror(error));
		return 0;
	} else {
		return 1;
	}
}

void driver_end(void)
{
	pa_simple_free(s);
	return;
}

void driver_frame(signed short *data, int len)
{
	int error;

	/* now in bytes */
	len <<= 1;

	if (pa_simple_write(s, data, (size_t) len, &error) < 0) {
		fprintf(stderr, "%s\n", pa_strerror(error));
	}

	/*if (pa_simple_drain(s, &error) < 0){
		fprintf(stderr,"%s\n",pa_strerror(error));
	}*/
	return;
}

