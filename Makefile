# makefile for aylet

# You need an ANSI C compiler. gcc is probably best.
#
CC	= gcc

# Compiler options.
#
CFLAGS	= -O2 -Wall -Wextra -pedantic

# Install directories
#
DESTDIR ?=
PREFIX	?= $(DESTDIR)/usr/local
BINDIR	= $(PREFIX)/bin
XBINDIR	= $(BINDIR)
MANDIR	= $(PREFIX)/man/man1

# if you want the X version to be installed in the usual X executables
# directory, uncomment this (altering if necessary):
#
#XBINDIR=/usr/X11R6/bin


# you shouldn't need to edit the rest
#-----------------------------------------------------------------

# this looks wrong, but *ops.c are actually #include'd by z80.c
OBJS-OSS=main.o sound.o ui.o z80.o drv-oss.o
OBJS-OBSD=main.o sound.o ui.o z80.o drv-obsd.o
OBJS-PULSE=main.o sound.o ui.o z80.o drv-pulse.o
GTKOBJS=main.o sound.o uigtk.o z80.o drv-pulse.o
GTK2OBJS=main.o sound.o uigtk2.o z80.o drv-pulse.o
GTK3OBJS=main.o sound.o uigtk3.o z80.o drv-pulse.o

all:
	@echo "make oss - build aylet with OSS driver"
	@echo "make obsd - build aylet with OpenBSD driver"
	@echo "make pulse - build aylet with Pulseaudio driver"

oss: aylet-oss
obsd: aylet-obsd
pulse: aylet-pulse

aylet-oss: $(OBJS-OSS)
	$(CC) -o aylet $(OBJS-OSS) -lncurses

aylet-obsd: $(OBJS-OBSD)
	$(CC) -o aylet $(OBJS-OBSD) -lcurses

aylet-pulse: $(OBJS-PULSE)
	$(CC) -o aylet $(OBJS-PULSE) -lncurses `pkg-config --libs libpulse-simple`

xaylet-gtk: $(GTKOBJS)
	$(CC) -o xaylet-gtk $(GTKOBJS) `gtk-config --libs` `pkg-config --libs libpulse-simple`

xaylet-gtk2: $(GTK2OBJS)
	$(CC) -o xaylet-gtk2 $(GTK2OBJS) `pkg-config --libs gtk+-2.0` `pkg-config --libs libpulse-simple`

xaylet-gtk3: $(GTK3OBJS)
	$(CC) -o xaylet-gtk3 $(GTK3OBJS) `pkg-config --libs gtk+-3.0` `pkg-config --libs libpulse-simple`

uigtk.o: uigtk.c
	$(CC) $(CFLAGS) `gtk-config --cflags` -c uigtk.c -o uigtk.o

uigtk2.o: uigtk2.c
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-2.0` -c uigtk2.c -o uigtk2.o

uigtk3.o: uigtk3.c
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-3.0` -c uigtk3.c -o uigtk3.o

installdirs:
	/bin/sh ./mkinstalldirs $(BINDIR) $(MANDIR)

install: installdirs
	if [ -f aylet ]; then install -m 755 aylet $(BINDIR); fi
	if [ -f xaylet-gtk ]; then install -m 755 xaylet-gtk $(XBINDIR); fi
	if [ -f xaylet-gtk2 ]; then install -m 755 xaylet-gtk2 $(XBINDIR); fi
	if [ -f xaylet-gtk3 ]; then install -m 755 xaylet-gtk3 $(XBINDIR); fi
	install -m 644 aylet.1 $(MANDIR)
	ln -sf $(MANDIR)/aylet.1 $(MANDIR)/xaylet.1

uninstall:
	$(RM) $(BINDIR)/aylet $(XBINDIR)/xaylet-gtk $(XBINDIR)/xaylet-gtk2
	$(RM) $(BINDIR)/xaylet-gtk3
	$(RM) $(MANDIR)/aylet.1* $(MANDIR)/xaylet.1*

clean:
	$(RM) *.o *~ aylet xaylet-gtk xaylet-gtk2 xaylet-gtk3

# dependencies
cbops.o: cbops.c
drv-obsd.o: drv-obsd.c
drv-oss.o: drv-oss.c
drv-pulse.o: drv-pulse.c
edops.o: edops.c
main.o: main.c main.h sound.h ui.h z80.h
sound.o: sound.c main.h z80.h sound.h driver.h
ui.o: ui.c main.h ui.h
uigtk.o: uigtk.c main.h ui.h button1.xpm button2.xpm button3.xpm \
button4.xpm button5.xpm
uigtk2.o: uigtk2.c main.h ui.h
uigtk3.o: uigtk3.c main.h ui.h
z80.o: z80.c main.h z80.h z80ops.c cbops.c edops.c
z80ops.o: z80ops.c cbops.c edops.c


VERS=0.5

tgz: ../aylet-$(VERS).tar.gz

../aylet-$(VERS).tar.gz: clean
	$(RM) ../aylet-$(VERS)
	@cd ..;ln -s aylet aylet-$(VERS)
	cd ..;tar zchvf aylet-$(VERS).tar.gz aylet-$(VERS)
	@cd ..;$(RM) aylet-$(VERS)
