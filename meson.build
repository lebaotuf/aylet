project('aylet', 'c', version: '0.5')

common_sources = ['main.c', 'sound.c', 'z80.c']
curses_sources = ['ui.c']
gtk1_sources = ['uigtk.c']
gtk2_sources = ['uigtk2.c']
gtk3_sources = ['uigtk3.c']

compiler = meson.get_compiler('c')

snddrv_opt = get_option('snddrv')
ui_opt = get_option('ui')
if snddrv_opt == 'pulse'
  snddrv_sources = ['drv-pulse.c']
  snddrv_dep = dependency('libpulse-simple')
elif snddrv_opt == 'oss'
  if compiler.check_header('sys/soundcard.h')
    snddrv_sources = ['drv-oss.c']
    snddrv_dep = dependency('', required: false)
  else
    error('Missing header: sys/soundcard.h')
  endif
elif snddrv_opt == 'obsd'
  if compiler.check_header('sys/audioio.h')
    snddrv_sources = ['drv-obsd.c']
    snddrv_dep = dependency('', required: false)
  else
    error('Missing header: sys/audioio.h')
  endif
endif

ncurses_dep = dependency('ncurses')
executable('aylet',
	   [common_sources, snddrv_sources, curses_sources],
	   dependencies : [snddrv_dep, ncurses_dep])

if ui_opt.contains('gtk1')
  ui_dep = dependency('gtk+')
  executable('xaylet-gtk',
	     [common_sources, snddrv_sources, gtk1_sources],
	     dependencies : [snddrv_dep, ui_dep])
endif

if ui_opt.contains('gtk2')
  ui_dep = dependency('gtk+-2.0')
  executable('xaylet-gtk2',
	     [common_sources, snddrv_sources, gtk2_sources],
	     dependencies : [snddrv_dep, ui_dep])
endif

if ui_opt.contains('gtk3')
  ui_dep = dependency('gtk+-3.0')
  executable('xaylet-gtk3',
	     [common_sources, snddrv_sources, gtk3_sources],
	     dependencies : [snddrv_dep, ui_dep])
endif
