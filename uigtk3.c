/* aylet 0.5, a .AY music file player.
 * Copyright (C) 2023 Ábel Futó
 * See main.c for licence.
 *
 * uigtk3.c - GTK+3 UI code.
 */

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "main.h"

#include "ui.h"

static GtkWidget *window, *vbox, *detailsgrid, *detaillabel[5];
static GtkWidget *label_for_status, *label_for_time;
static GtkWidget *highspeed_widget;
static GtkWidget *label_for_stopafter, *label_for_fadetime;

static int used_ui = 0, need_update = 1;
static int retval = 1;


static void action(enum cb_action_tag a)
{
	/* only set retval if we get a zero return, i.e. we need to
	 * stop current track.
	 */
	if(!action_callback(a))
		retval = 0;
}


static void cb_button_prev_track(void) { action(cb_prev_track); }
static void cb_button_next_track(void) { action(cb_next_track); }
static void cb_button_play(void) { action(cb_play); }
static void cb_button_pause(void) { action(cb_pause); }
static void cb_button_stop(void) { action(cb_stop); }
static void cb_button_restart(void) { action(cb_restart); }
static void cb_button_prev_file(void) { action(cb_prev_file); }
static void cb_button_next_file(void) { action(cb_next_file); }

static void cb_toggle_highspeed(void)
{
	action(cb_highspeed);
}

static void cb_doquit(void)
{
	window = NULL;
	action(cb_quit);
}


/* shouldn't really be needing this, but I had trouble
 * with a few bits and pieces.
 */
gint keypress(GtkWidget *widget, GdkEventKey *event)
{
	switch(event->keyval) {
	case GDK_KEY_BackSpace: case GDK_KEY_Delete:
		action(cb_prev_file);
		break;

	case GDK_KEY_space:
		action(cb_next_file);
		break;

	case GDK_KEY_q: case GDK_KEY_Escape:
		cb_doquit();
		break;

	case GDK_KEY_s: case GDK_KEY_S:
		action((event->state & GDK_SHIFT_MASK)?cb_dec_stopafter:cb_inc_stopafter);
		break;
    
	case GDK_KEY_f: case GDK_KEY_F:
		action((event->state & GDK_SHIFT_MASK)?cb_dec_fadetime:cb_inc_fadetime);
		break;
    
	default:
		return(FALSE);	/* don't stop event if not handled */
	}

	/* if we handled it, stop anything else getting the event. */
	g_signal_stop_emission_by_name(G_OBJECT(widget), "key_press_event");

	return(TRUE);
}


static void ui_initwin(void)
{
	GtkWidget *statusgrid, *buttongrid;
	GtkWidget *sep, *label, *hbox, *button, *ebox;
	GtkWidget *pixmap;
	int tbl_row;

	GtkAccelGroup *accel;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(G_OBJECT(window), "destroy",
			 G_CALLBACK(cb_doquit), NULL);
	g_signal_connect(G_OBJECT(window), "key_press_event",
			 G_CALLBACK(keypress), NULL);
	gtk_widget_set_events(window, GDK_KEY_PRESS_MASK);
	gtk_window_set_title(GTK_WINDOW(window), "xaylet");
	gtk_window_set_resizable(GTK_WINDOW(window), FALSE);

	accel = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW (window), accel);

	/* main vbox */
	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
	gtk_widget_show(vbox);

	/* Metadata grid */
	/* left one is for description (e.g. "Filename:"),
	 * right one for value.
	 */
	detailsgrid = gtk_grid_new();
	gtk_box_pack_start(GTK_BOX(vbox), detailsgrid, TRUE, TRUE, 0);
	gtk_widget_set_margin_start(detailsgrid, 10);
	gtk_grid_set_column_spacing(GTK_GRID(detailsgrid), 10);
	gtk_grid_set_row_spacing(GTK_GRID(detailsgrid), 2);
	gtk_container_set_border_width(GTK_CONTAINER(detailsgrid), 5);
	gtk_widget_show(detailsgrid);

#define DO_TBL_LEFT(grid, col, row, name)				\
	label = gtk_label_new(name);					\
	gtk_label_set_xalign(GTK_LABEL(label), 1.);			\
	gtk_label_set_yalign(GTK_LABEL(label), 0.5);			\
	gtk_grid_attach(GTK_GRID(grid), label,				\
			(col), (row), 1, 1);				\
	gtk_widget_show(label)

#define DO_TBL_RIGHT(grid, col, row, name)				\
	ebox = gtk_event_box_new();					\
	label = gtk_label_new(name);					\
	gtk_label_set_xalign(GTK_LABEL(label), 0.);			\
	gtk_label_set_yalign(GTK_LABEL(label), 0.5);			\
	gtk_container_add(GTK_CONTAINER(ebox), label);			\
	gtk_widget_set_size_request(label, 5, 5);			\
	gtk_grid_attach(GTK_GRID(grid), ebox,				\
			(col), (row), 1, 1);				\
	gtk_widget_show(label), gtk_widget_show(ebox)

	tbl_row = 0;
	DO_TBL_LEFT(detailsgrid, 0, tbl_row, "File:");
	DO_TBL_RIGHT(detailsgrid, 1, tbl_row, "");
	detaillabel[tbl_row] = label;
	tbl_row++;
	DO_TBL_LEFT(detailsgrid, 0, tbl_row,"Misc:");
	DO_TBL_RIGHT(detailsgrid, 1, tbl_row,"");
	detaillabel[tbl_row] = label;
	tbl_row++;
	DO_TBL_LEFT(detailsgrid, 0, tbl_row, "Author:");
	DO_TBL_RIGHT(detailsgrid, 1, tbl_row, "");
	detaillabel[tbl_row] = label;
	tbl_row++;
	DO_TBL_LEFT(detailsgrid, 0, tbl_row, "Tracks:");
	DO_TBL_RIGHT(detailsgrid, 1, tbl_row, "");
	detaillabel[tbl_row] = label;
	tbl_row++;
	DO_TBL_LEFT(detailsgrid, 0, tbl_row, "Playing:");
	DO_TBL_RIGHT(detailsgrid, 1, tbl_row, "");
	detaillabel[tbl_row] = label;

	sep = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_box_pack_start(GTK_BOX(vbox), sep, TRUE, TRUE, 0);
	gtk_widget_show(sep);


	/* Status grid */
	statusgrid = gtk_grid_new();
	gtk_box_pack_start(GTK_BOX(vbox), statusgrid, TRUE, TRUE, 0);
	gtk_grid_set_column_spacing(GTK_GRID(statusgrid), 10);
	gtk_grid_set_column_homogeneous(GTK_GRID(statusgrid), TRUE);
	gtk_grid_set_row_spacing(GTK_GRID(statusgrid), 2);
	gtk_container_set_border_width(GTK_CONTAINER(statusgrid), 5);
	gtk_widget_show(statusgrid);

	DO_TBL_LEFT(statusgrid, 0, 0, "Status:");
	DO_TBL_RIGHT(statusgrid, 1, 0, "");
	label_for_status = label;
	DO_TBL_LEFT(statusgrid, 0, 1, "Time:");
	DO_TBL_RIGHT(statusgrid, 1, 1, "");
	label_for_time = label;

	highspeed_widget = gtk_check_button_new_with_mnemonic ("_High speed");
	/* don't allow focus, looks too weird and we have shortcut */

	gtk_widget_set_can_focus(highspeed_widget, FALSE);
	gtk_grid_attach(GTK_GRID(statusgrid), highspeed_widget, 2, 0, 1, 2);
	g_signal_connect(G_OBJECT(highspeed_widget), "clicked",
			   G_CALLBACK(cb_toggle_highspeed), NULL);
	gtk_widget_add_accelerator(highspeed_widget, "clicked",
				   accel,
				   GDK_KEY_h, 0, 0);
	gtk_widget_show(highspeed_widget);


	/* XXX these should really be spin buttons */
	DO_TBL_LEFT(statusgrid, 3, 0, "Stop after:");
	DO_TBL_RIGHT(statusgrid, 4, 0, "");
	label_for_stopafter = label;
	DO_TBL_LEFT(statusgrid, 3, 1, "Fade time:");
	DO_TBL_RIGHT(statusgrid, 4, 1, "");
	label_for_fadetime = label;

	sep = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_box_pack_start(GTK_BOX(vbox), sep, TRUE, TRUE, 0);
	gtk_widget_show(sep);


	buttongrid = gtk_grid_new();
	gtk_box_pack_start(GTK_BOX(vbox), buttongrid, TRUE, TRUE, 0);
	gtk_grid_set_column_spacing(GTK_GRID(buttongrid), 5);
	gtk_grid_set_column_homogeneous(GTK_GRID(buttongrid), TRUE);
	gtk_grid_set_row_spacing(GTK_GRID(buttongrid), 5);
	gtk_container_set_border_width(GTK_CONTAINER(buttongrid), 5);
	gtk_widget_show(buttongrid);

#define ADD_PIXMAP_TO_BUTTON(button, xpm, iconname)				\
	pixmap = gtk_image_new_from_icon_name(iconname, GTK_ICON_SIZE_BUTTON);	\
	gtk_container_add(GTK_CONTAINER((button)), pixmap);			\
	gtk_widget_show(pixmap)

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_grid_attach(GTK_GRID(buttongrid), hbox, 0, 0, 4, 1);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 0);
	gtk_widget_show(hbox);

	/* XXX this seems to be needed here, but is it ok? */
	gtk_widget_realize(window);

	button = gtk_button_new();
	ADD_PIXMAP_TO_BUTTON(button, button1, "media-skip-backward");
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(cb_button_prev_track), NULL);
	gtk_widget_add_accelerator(button, "clicked", accel,
				   GDK_KEY_z, 0, 0);
	/* since I use Space, there's no point having these focusable... */
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);

	button = gtk_button_new();
	ADD_PIXMAP_TO_BUTTON(button, button2, "media-playback-start");
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(cb_button_play), NULL);
	gtk_widget_add_accelerator(button, "clicked", accel,
				   GDK_KEY_x, 0, 0);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);

	button = gtk_button_new();
	ADD_PIXMAP_TO_BUTTON(button, button3, "media-playback-pause");
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(cb_button_pause), NULL);
	gtk_widget_add_accelerator(button, "clicked", accel,
				   GDK_KEY_c, 0, 0);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);

	button = gtk_button_new();
	ADD_PIXMAP_TO_BUTTON(button, button4, "media-playback-stop");
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(cb_button_stop), NULL);
	gtk_widget_add_accelerator(button,"clicked", accel,
				   GDK_KEY_v, 0, 0);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);

	button = gtk_button_new();
	ADD_PIXMAP_TO_BUTTON(button, button5, "media-skip-forward");
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(cb_button_next_track), NULL);
	gtk_widget_add_accelerator(button, "clicked", accel,
				   GDK_KEY_b, 0, 0);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);

	button = gtk_button_new_with_mnemonic("_Restart");
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(cb_button_restart), NULL);
	gtk_widget_add_accelerator(button, "clicked", accel,
				   GDK_KEY_r, 0, 0);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);


	/* second row, these go straight on the grid */
	/* backspace/delete/space are dealt with by keypress() */
	button = gtk_button_new_with_mnemonic("_Previous file");
	gtk_grid_attach(GTK_GRID(buttongrid), button, 1, 1, 1, 1);
	g_signal_connect(G_OBJECT(button), "clicked",
			   G_CALLBACK(cb_button_prev_file), NULL);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);

	button = gtk_button_new_with_mnemonic("_Next file");
	gtk_grid_attach(GTK_GRID(buttongrid),button, 2, 1, 1, 1);
	g_signal_connect(G_OBJECT(button), "clicked",
			   G_CALLBACK(cb_button_next_file), NULL);
	gtk_widget_set_can_focus(button, FALSE);
	gtk_widget_show(button);


	gtk_widget_show(window);
}


static void ui_draw_status(char *filename, char *misc,
                           char *author, int track, char *playingstr)
{
	static gchar buf[256];
	char *ptr = strrchr(filename,'/');
	int tbl_row = 0;

	if(!window) return;

	gtk_label_set_text(GTK_LABEL(detaillabel[tbl_row]), ptr?ptr+1:filename);
	tbl_row++;
	gtk_label_set_text(GTK_LABEL(detaillabel[tbl_row]), misc);
	tbl_row++;
	gtk_label_set_text(GTK_LABEL(detaillabel[tbl_row]), author);
	tbl_row++;
	g_snprintf(buf,sizeof(buf),"%d", aydata.num_tracks);
	gtk_label_set_text(GTK_LABEL(detaillabel[tbl_row]), buf);
	tbl_row++;
	g_snprintf(buf,sizeof(buf),"%d - %s", track, playingstr);
	gtk_label_set_text(GTK_LABEL(detaillabel[tbl_row]), buf);

	gtk_label_set_text(GTK_LABEL(label_for_status),
			   paused?"paused ":(playing?"playing":"stopped"));

	if(!stopafter) {
		gtk_label_set_text(GTK_LABEL(label_for_stopafter), "--:--");
	} else {
		g_snprintf(buf,sizeof(buf), "%2d:%02d", stopafter/60, stopafter%60);
		gtk_label_set_text(GTK_LABEL(label_for_stopafter), buf);
	}

	g_snprintf(buf, sizeof(buf), "%2d sec", fadetime);
	gtk_label_set_text(GTK_LABEL(label_for_fadetime), buf);
}


static void ui_draw_status_timeonly(void)
{
	static char buf[32];

	if(!window) return;

	g_snprintf(buf, sizeof(buf), "%2d:%02d", tunetime.min, tunetime.sec);
	gtk_label_set_text(GTK_LABEL(label_for_time), buf);
}


/* called per 1/50th, this deals with the usual events.
 * returns zero if we want to stop the current track.
 */
int ui_frame(void)
{
	if(!window) return(0);

	retval=1;

	if(need_update) {
		need_update = 0;
		ui_draw_status(ay_filenames[ay_file], (char *)aydata.miscstr, (char *)aydata.authorstr,
			       ay_track+1, (char *) aydata.tracks[ay_track].namestr);
	}

	/* update time display */
	ui_draw_status_timeonly();

	/* this should be the only place retval gets modified by actions */
	while(gtk_events_pending())
		gtk_main_iteration();

	return(retval);
}


/* called if playback status has changed without us knowing. */
void ui_change_notify(void)
{
	need_update = 1;
}


void ui_init(int argc,char **argv)
{
	used_ui = 1;

	if(!use_ui) {
		fprintf(stderr, "xaylet: warning: "
			"non-UI mode not supported by GTK+ version, use `aylet'.\n");
		use_ui = 1;
	}

	gtk_init(&argc, &argv);
	ui_initwin();
}


void ui_end(void)
{
	if(!used_ui) return;

	/* XXX can't call gtk_exit(), ui_end() is meant to return!
	 * Oh well, it seems to be coping without... :-/
	 */
}
